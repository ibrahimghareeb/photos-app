import 'package:flutter/material.dart';

class ImageText{
  String value;
  Offset offset = Offset.zero;
  double? fontSize;
  Color? fontColor;
  TextAlign? textAlign;
  FontWeight? fontWeight;
  FontStyle? fontStyle;
  TextStyle? textStyle;

  ImageText({required this.value, this.fontSize, this.fontColor,this.fontStyle,this.fontWeight,this.textAlign,this.textStyle});
}