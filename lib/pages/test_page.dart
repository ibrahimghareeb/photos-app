// import 'dart:math';
//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:photo_view/photo_view.dart';
// class TestPage extends StatefulWidget {
//   double imageWidth=0;
//   double imageHeight=0;
//   @override
//   State<TestPage> createState() => _TestPageState();
// }
//
// class _TestPageState extends State<TestPage> {
//   late PhotoViewScaleStateController scaleStateController;
//   ScaleUpdateDetails temp=ScaleUpdateDetails();
//   @override
//   Widget build(BuildContext context) {
//     PhotoViewControllerBase<PhotoViewControllerValue> controller;
//     double width=MediaQuery.of(context).size.width;
//     double height=MediaQuery.of(context).size.height;
//     if( widget.imageWidth==widget.imageHeight){
//       widget.imageHeight=height*0.4;
//       widget.imageWidth=width;
//     }
//     return Scaffold(
//       appBar: AppBar(title:const Text("rotate"),),
//       body:  Container(
//         width: width,
//         height: height,
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             GestureDetector(
//               onScaleUpdate: (update){
//
//                 setState(() {
//                   print(update.toString());
//                   if(update.horizontalScale>temp.horizontalScale)
//                     widget.imageWidth=widget.imageWidth+update.horizontalScale;
//                   if(update.horizontalScale<temp.horizontalScale)
//                     widget.imageWidth=widget.imageWidth-update.horizontalScale;
//                   if(update.verticalScale>temp.verticalScale)
//                     widget.imageHeight=widget.imageHeight+update.verticalScale;
//                   if(update.verticalScale<temp.verticalScale)
//                     widget.imageHeight=widget.imageHeight-update.verticalScale;
//                   temp=update;
//                 });
//               },
//               child: SingleChildScrollView(scrollDirection: Axis.horizontal,child: SingleChildScrollView(child: Container(child: Image.asset("assets/images/iphone.jpg",width: widget.imageWidth,height: widget.imageHeight,fit: BoxFit.fill,)))),
//             )
//           ],
//         ),
//       )
//     );
//   }
// }

import 'package:flutter/material.dart';

class TextOverImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Text Over Image Image Example'),
      ),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.blue,
                    image: DecorationImage(
                        image: new NetworkImage(
                            "https://thumbs.dreamstime.com/b/funny-face-baby-27701492.jpg"),
                        fit: BoxFit.fill)),
              ),
              HomePage()
            ],
          ),
        ),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Offset offset = Offset.zero;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Positioned(
        left: offset.dx,
        top: offset.dy,
        child: GestureDetector(
            onPanUpdate: (details) {
              setState(() {
                offset = Offset(
                    offset.dx + details.delta.dx, offset.dy + details.delta.dy);
              });
            },
            child: SizedBox(
              width: 300,
              height: 300,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text("You Think You Are Funny But You Are Not",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 28.0,
                          color: Colors.red)),
                ),
              ),
            )),
      ),
    );
  }
}