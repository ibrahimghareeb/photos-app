// import 'dart:math';
// import 'dart:typed_data';
// import 'package:burgas_photo/model/text_model.dart';
// import 'package:burgas_photo/widget/blocker_pick.dart';
// import 'package:burgas_photo/widget/dialog.dart';
// import 'package:flutter/material.dart';
// import 'dart:ui' as ui;
// import 'dart:io';
// import 'package:flutter/services.dart';
// import 'package:flutter/rendering.dart';
// import 'package:image_gallery_saver/image_gallery_saver.dart';
// import 'package:permission_handler/permission_handler.dart';
// import 'package:screenshot/screenshot.dart';
//
// class MergePage extends StatefulWidget {
//   File firstImageCropped;
//   File secondImageCropped;
//   MergePage(this.firstImageCropped,this.secondImageCropped) {
//     // final image1 = decodeImage(firstImageCropped!.readAsBytesSync());
//     // final image2 = decodeImage(secondImageCropped!.readAsBytesSync());
//     // final mergedImage = MyImage.Image(image1!.width + image2!.width, max(image1.height, image2.height));
//     // copyInto(mergedImage, image1, blend :false);
//     // copyInto(mergedImage, image2, dstX : image1.width, blend : false);
//     // image=mergedImage;
//   }
//
//   @override
//   State<MergePage> createState() => _MergePageState();
// }
//
// class _MergePageState extends State<MergePage> {
//   //List<ui.Image> list=[];
//   late double width;
//   late double height;
//   final screenController=ScreenshotController();
//   TextEditingController text=TextEditingController();
//   TextEditingController fontSize=TextEditingController();
//   List<ImageText> texts=[];
//
//   // Future<File?> cropPhoto(XFile imageFile)async  {
//   //   File? cropped=await ImageCropper().cropImage(
//   //       sourcePath: imageFile.path,
//   //       compressFormat: ImageCompressFormat.jpg,
//   //       androidUiSettings:const AndroidUiSettings(
//   //         toolbarTitle: 'Cropper',
//   //         toolbarColor: Colors.deepOrange,
//   //         toolbarWidgetColor: Colors.white,
//   //         initAspectRatio: CropAspectRatioPreset.original,
//   //         lockAspectRatio: false
//   //       ),
//   //     compressQuality: 100);
//   //   //File? cropped=await ImageEditor.editFileImageAndGetFile(file: File( imageFile.path), imageEditorOption: ImageEditorOption());
//   //   //imageFile=File(cropped!.path) as XFile?;
//   //   if(cropped==null){
//   //     return null;
//   //   }else {
//   //     return cropped;
//   //   }
//   // }
//
//
//
//   @override
//   Widget build(BuildContext context) {
//     width=MediaQuery.of(context).size.width;
//     height=MediaQuery.of(context).size.height;
//     return Scaffold(
//       floatingActionButton: FloatingActionButton(
//         child: Icon(Icons.add),
//         onPressed: (){
//           showDialog(context: context, builder:  (BuildContext context) {
//             return dialog();
//           },);
//         },
//       ),
//       appBar: AppBar(
//         title: const Text("Mix Photo"),
//       ),
//       body: SingleChildScrollView(
//         child: Container(
//           width: width,
//           height: height,
//           child: Column(
//             children: [
//               Screenshot(
//                 controller: screenController,
//                 child: Container(
//                   width: width,
//                   height: height*0.5,
//                   child: InteractiveViewer(
//                     child: Stack(
//                       children: [
//                         Image.file(widget.firstImageCropped,width: width,height: height*0.5,fit: BoxFit.fill,),
//                         Image.file(widget.secondImageCropped,width: width,height: height*0.5,fit: BoxFit.fill,),
//                         for(int i=0;i<texts.length;i++)
//                           Positioned(
//                               top: texts[i].top,
//                               left: texts[i].left,
//                               child: //Draggable(
//                                 //feedback: Text(texts[i].value, style: TextStyle(fontSize: texts[i].fontSize, color: texts[i].fontColor),),
//                                 //child: Text(texts[i].value, style: TextStyle(fontSize: texts[i].fontSize, color: texts[i].fontColor),),
//                                 onDragEnd: (drag){
//                                   final renderBox=context.findRenderObject() as RenderBox;
//                                   Offset offset=renderBox.globalToLocal(drag.offset);
//                                   setState(() {
//                                     texts[i].left=offset.dx;
//                                     texts[i].top=offset.dy;
//                                   });
//                                 },
//                               )
//                           ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//               MaterialButton(
//                 onPressed: () async {
//                   var status = await Permission.storage.status;
//                   if (!status.isGranted) {
//                     await Permission.storage.request();
//                   }else
//                     takeScreenShot();
//                 },
//                 color: Colors.blue,
//                 minWidth: width*0.3,
//                 child: Text("Save",style: TextStyle(color: Colors.white),),
//               ),
//             ],
//           ),
//         ),
//       )
//     );
//   }
//   Future<void>takeScreenShot() async{
//     final image = await screenController.capture();
//     //final directory = (await getApplicationDocumentsDirectory()).path;
//
//     var dt = DateTime.now();
//     // String dir='/storage/emulated/0/Burgas Photo/';
//     // if(!await File(dir).exists()){
//     //   final path=Directory('$dir');
//     //   path.create();
//     //   //print("done");
//     // }
//
//     // File imgFile =new File('/storage/emulated/0/Burgas Photo/screenshot '+dt.toString()+'.png');
//
//     final result=await ImageGallerySaver.saveImage(image!,name:"screenshoot+${dt.toString()}" ,quality: 100);
//     if(result!=null){
//       print (result['filePath']);
//     }
//   }
//
//   Widget dialog(){
//     late Color selectedColor;
//     return AlertDialog(
//
//       elevation: 10,
//       title: Text("Add new Text"),
//       shape: OutlineInputBorder(
//           borderRadius: BorderRadius.all(Radius.circular(16)),
//           borderSide: BorderSide.none
//       ),
//       content: Container(
//         height: height*0.3,
//         child: Column(
//           children: [
//             TextFormField(
//               controller: text,
//               decoration: InputDecoration(
//                 hintText: "Enter your text"
//               ),
//             ),
//             Container(
//               width: width*0.3,
//               child: TextFormField(
//                 controller: fontSize,
//                 decoration: InputDecoration(
//                     hintText: "Enter font size"
//                 ),
//               ),
//             ),
//             Container(height:height*0.1,child: BlockPicker(pickerColor: Colors.white, onColorChanged: (color){selectedColor=color;},availableColors: [Colors.black,Colors.red,Colors.grey,Colors.green],)),
//           ],
//         )
//
//       ),
//       actions: [
//         MaterialButton(
//           onPressed: (){
//             setState(() {
//               texts.add(ImageText(fontColor: selectedColor,fontSize: double.parse(fontSize.text.toString() ),value: text.text.toString(),left: 0,top: 0));
//               Navigator.pop(context);
//             });
//           },
//           child: const Text("Add"),
//         )
//       ],
//     );
//   }
// }
// //$directory

// Widget saveOptionDialog(){
//   return AlertDialog(
//     elevation: 10,
//     title: Text("Save as"),
//     content: Container(
//       height: height!*0.15,
//       child: Column(
//         children: [
//           MaterialButton(
//             minWidth: width,
//             onPressed: (){
//               setState(() {
//                 imageheight=height!*0.5;
//                 Navigator.pop(context);
//                 takeScreenShot();
//               });
//             },
//             child: Row(children: [Text("Facebook post")],),
//           ),
//           Divider(height: 1,),
//           MaterialButton(
//             minWidth: width,
//             onPressed: (){
//               setState(() {
//                 imageheight=height;
//               });
//               print(height);
//               print(imageheight);
//               Navigator.pop(context);
//               //takeScreenShot();
//             },
//             child: Row(children: [Text("Instagram story")],),
//           ),
//         ],
//       ),
//     ),
//
//   );
// }