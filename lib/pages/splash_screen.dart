import 'package:burgas_photo/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    Future.delayed(Duration(seconds: 2)).then((value) => Navigator.of(context).pushAndRemoveUntil(PageTransition(child: HomePage(), type: PageTransitionType.rightToLeft),ModalRoute.withName("/login")));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration:const BoxDecoration(
        image: DecorationImage(image: AssetImage("assets/images/splash.png"),fit: BoxFit.fill)
      ),

    );
  }
}
