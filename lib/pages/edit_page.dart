import 'dart:io' as io;
import 'dart:typed_data';
import 'package:image/image.dart' as ImageLibeary;
import 'package:burgas_photo/model/text_model.dart';
import 'package:burgas_photo/widget/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_font_picker/flutter_font_picker.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mdi/mdi.dart';
import 'package:screenshot/screenshot.dart';

class EditPage extends StatefulWidget {
  EditPage({Key? key,this.firstImageFile,this.height,this.width}) { print(firstImageFile!.path);}
  
  XFile? firstImageFile;
  XFile? secondImageFile;
  bool isFirstImageSelected=true;
  bool isSecondeImageSelected=false;
  double? width;
  double? height;

  @override
  State<EditPage> createState() => _EditPageState(width!,height!);
}

class _EditPageState extends State<EditPage> {

  _EditPageState(this.imageWidth, this.imageHeight);

  int selectedIndex=0;
  final screenController=ScreenshotController();
  double horizontalScale=1;
  double verticalScale=1;
  //double? imageheight;
  List<ImageText> texts=[];
  double fontSize=10;
  String selectedFont = "Roboto";
  Matrix4? transform;
  Matrix4? scale;
  ScaleUpdateDetails temp=ScaleUpdateDetails();
  double angle=0;
  double imageWidth;
  double imageHeight;
  TextEditingController text=TextEditingController();
  bool alignLeft=true;
  bool alignRight=false;
  bool alignCenter=false;
  bool isBold=false;
  bool isItalic=false;
  Color selectedColor=Colors.black;
  TextStyle selectedFontTextStyle=TextStyle();
  //ImageText? textToEdit;
  int indexEditText=-1;
  final key=GlobalKey();

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: selectedIndex,
        selectedItemColor: Color.fromRGBO(255, 45, 85, 1),
        onTap: (val){
          setState(() {
            selectedIndex=val;
          });
        },
        items:const [
          BottomNavigationBarItem(icon: Icon(Mdi.imageSizeSelectActual),label: "Edit",),
          BottomNavigationBarItem(icon: Icon(Icons.abc),label: "Text"),
          BottomNavigationBarItem(icon: Icon(Mdi.contentSave),label: "Save")
        ],
      ),
      body: SafeArea(
        child: SizedBox(
          width: width,
          height: height,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width:width,
                  height: widget.height,
                  color: Colors.grey[300],
                  child: Screenshot(
                      controller: screenController,
                      child: Stack(
                        children: [
                          GestureDetector(
                              onScaleUpdate: (update){
                                setState(() {
                                  if(update.horizontalScale>temp.horizontalScale)
                                    imageWidth=imageWidth+update.horizontalScale;
                                  if(update.horizontalScale<temp.horizontalScale)
                                    imageWidth=imageWidth-update.horizontalScale;
                                  if(update.verticalScale>temp.verticalScale)
                                    imageHeight=imageHeight+update.verticalScale;
                                  if(update.verticalScale<temp.verticalScale)
                                    imageHeight=imageHeight-update.verticalScale;
                                  temp=update;
                                });
                              },
                              child: Center(child: SingleChildScrollView(scrollDirection: Axis.horizontal,child: SingleChildScrollView(child: Column(children: [Transform.rotate(angle: angle,child: Image.file(io.File(widget.firstImageFile!.path),width: imageWidth,height: imageHeight,fit: BoxFit.fill,))],))))),
                          widget.isSecondeImageSelected?Image.file(io.File(widget.secondImageFile!.path),width: width,height: imageHeight,fit: BoxFit.fill ,):Container(),
                          for(int i=0;i<texts.length;i++)
                            Positioned(
                                top: texts[i].offset.dy,
                                left: texts[i].offset.dx,
                                child: GestureDetector(
                                    onPanUpdate: (details) {
                                      setState(() {
                                        double dx,dy;
                                        dx=texts[i].offset.dx + details.delta.dx<=width-key.currentContext!.size!.width&&texts[i].offset.dx + details.delta.dx>0?texts[i].offset.dx + details.delta.dx:texts[i].offset.dx;
                                        dy=texts[i].offset.dy + details.delta.dy<=(height*0.52)-key.currentContext!.size!.height&&texts[i].offset.dy + details.delta.dy>0? texts[i].offset.dy + details.delta.dy:texts[i].offset.dy;
                                          texts[i].offset = Offset(dx, dy);
                                      });
                                      //print(texts[i].offset.dx);
                                    },
                                  onLongPress: (){
                                    if(selectedIndex==-1){
                                      return;
                                    }
                                    setState(() {
                                      text=TextEditingController(text: texts[i].value);
                                      selectedColor=texts[i].textStyle!.color!;
                                      indexEditText=i;
                                      selectedIndex=1;
                                    });
                                },
                                    child: Text(texts[i].value,key:key, style: texts[i].textStyle,textAlign: texts[i].textAlign,))
                            ),
                        ],
                      )
                  ),
                ),
                selectedIndex==0?resizePhoto(context):selectedIndex==1?textProperties():selectedIndex==2?saveOption(width,height):Container()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void>takeScreenShot(double width,double height) async{
    final image = await screenController.capture();
    var img = ImageLibeary.decodeImage(image!);
    ImageLibeary.Image finalImage=ImageLibeary.copyResize(img!,width: width.toInt(),height: height.toInt());
    var dt = DateTime.now();
    var tmp=ImageLibeary.encodePng(finalImage);
    // await new io.File('/storage/emulated/0/screenshoot+${dt.toString()}.png')
    //   ..writeAsBytesSync(ImageLibeary.encodePng(finalImage));
    //ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("saved! "),));
    final result=await ImageGallerySaver.saveImage(Uint8List.fromList(tmp),name:"screenshoot+${dt.toString()}" ,quality: 100, );
    if(result!=null){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("saved! "+result.toString()),
      ));
    }
  }

  Widget textProperties(){

    // if(textToEdit!=null){
    //   text.text=textToEdit!.value;
    //   fontSize=textToEdit!.textStyle!.fontSize!;
    //   selectedColor=textToEdit!.textStyle!.color!;
    //
    //   if(textToEdit!.textStyle!.fontWeight==FontWeight.bold){
    //     isBold=true;
    //   }
    //   if(textToEdit!.textStyle!.fontStyle==FontStyle.italic){
    //     isItalic=true;
    //   }
    // }
    return Container(
        padding: EdgeInsets.all(30),
        child: SingleChildScrollView(
          child: Column(
            children: [
              TextFormField(
                controller: text,
                decoration: const InputDecoration(
                  //label: textToEdit==null?Text(""):Text(textToEdit.value),
                    hintText: "Enter your text"
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height*0.02,),
              Row(
                children: [
                  Text("Font size"),
                  Expanded(
                    child: Slider(
                      onChanged: (val){
                        setState((){
                          indexEditText==-1?fontSize=val:texts[indexEditText].fontSize=val;
                        });
                      },
                      value: indexEditText==-1?fontSize:texts[indexEditText].fontSize!,
                      activeColor: Color.fromRGBO(255, 45, 85, 1),
                      thumbColor: Color.fromRGBO(255, 45, 85, 1),
                      inactiveColor: Color.fromRGBO(255, 45, 85, 0.1),
                      min: 10,
                      max: 80,
                    ),
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height*0.02,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                AppButton(
                  minWidth: MediaQuery.of(context).size.width*0.4,borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Pick Color", textColor: Colors.white, textSize: 15,
                  onPressed: (){
                    showDialog(context: context, builder: (BuildContext context){
                      return AlertDialog(
                        title: Text("Pick Color"),
                        content: SingleChildScrollView(
                          child: ColorPicker(
                            pickerColor: Color(0xff443a49),
                            onColorChanged: (Color value) {
                              selectedColor=value;
                            },
                          ),
                        ),
                        actions: [
                          MaterialButton(
                            onPressed: (){
                              Navigator.pop(context);
                            },
                            child: Text("Done"),
                          )
                        ],

                      );
                    });},
                ),
                AppButton(
                  minWidth: MediaQuery.of(context).size.width*0.4,borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Pick Family", textColor: Colors.white, textSize: 15,
                  onPressed: () {
                    showDialog(context: context, builder: (BuildContext context){
                      return AlertDialog(
                        content: SingleChildScrollView(
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            child: FontPicker(
                              googleFonts: myGoogleFonts,
                              showInDialog: true,
                              onFontChanged: (font){selectedFont=font.fontFamily;selectedFontTextStyle=font.toTextStyle();},
                            ),
                          ),
                        ),
                      );
                    });
                  },
                ),
              ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height*0.05,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: (){
                      setState(() {
                        alignLeft=true;
                        alignRight=false;
                        alignCenter=false;
                      });
                    },
                    child: Icon(Mdi.formatAlignLeft,color: alignLeft?Color.fromRGBO(255, 45, 85, 1):Colors.black),
                  ),
                  InkWell(
                    onTap: (){
                      setState(() {
                        alignLeft=false;
                        alignRight=false;
                        alignCenter=true;
                      });
                    },
                    child: Icon(Mdi.formatAlignCenter,color: alignCenter?Color.fromRGBO(255, 45, 85, 1):Colors.black,),
                  ),
                  InkWell(
                    onTap: (){
                      setState(() {
                        alignLeft=false;
                        alignRight=true;
                        alignCenter=false;
                      });
                    },
                    child: Icon(Mdi.formatAlignRight,color: alignRight?Color.fromRGBO(255, 45, 85, 1):Colors.black,),
                  ),
                  InkWell(
                    onTap: (){
                      setState(() {
                        isBold=!isBold;
                      });
                    },
                    child: Icon(Mdi.formatBold,color: isBold?Color.fromRGBO(255, 45, 85, 1):Colors.black),
                  ),
                  InkWell(
                    onTap: (){
                      setState(() {
                        isItalic=!isItalic;
                      });
                    },
                    child: Icon(Mdi.formatItalic,color: isItalic?Color.fromRGBO(255, 45, 85, 1):Colors.black),
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height*0.05,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppButton(minWidth: MediaQuery.of(context).size.width*0.4,borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText:  indexEditText==-1?"Add Text":"Edit Text", textColor: Colors.white, textSize: 15, onPressed: (){
                    setState(() {
                      TextAlign? textAlign;
                      if(alignCenter){
                        textAlign=TextAlign.center;
                      }
                      else if(alignRight){
                        textAlign=TextAlign.end;
                      }
                      else if(alignLeft){
                        textAlign=TextAlign.left;
                      }
                      if(indexEditText!=-1){
                        texts.elementAt(indexEditText).value=text.text.toString();
                        texts.elementAt(indexEditText).textStyle=selectedFontTextStyle.copyWith(fontSize:  indexEditText==-1?fontSize:texts[indexEditText].fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal);//TextStyle(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal,fontFamily: selectedFontTextStyle.fontFamily);
                        texts.elementAt(indexEditText).textAlign=textAlign;
                        text.clear();
                        fontSize=10;
                        indexEditText=-1;
                      }else {
                        texts.add(ImageText(
                            textAlign: textAlign,
                            value: text.text.toString(),
                            fontSize: fontSize,
                            fontColor: selectedColor,
                            fontStyle: isItalic?FontStyle.italic:FontStyle.normal,
                            fontWeight: isBold?FontWeight.bold:FontWeight.normal,
                            textStyle: selectedFontTextStyle.copyWith(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal)//TextStyle(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal,fontFamily: selectedFont)
                        ));
                        text.clear();
                        fontSize=10;

                      }
                  });
                  })
                ],
              )
            ],
          ),
        )

    );
      // actions: [
      //   MaterialButton(
      //     onPressed: (){
      //       setState(() {
      //         TextAlign? textAlign;
      //         if(alignCenter){
      //           textAlign=TextAlign.center;
      //         }
      //         else if(alignRight){
      //           textAlign=TextAlign.end;
      //         }
      //         else if(alignLeft){
      //           textAlign=TextAlign.left;
      //         }
      //         if(textToEdit!=null&&index!=null){
      //           texts.elementAt(index).value=text.text.toString();
      //           texts.elementAt(index).textStyle=selectedFontTextStyle.copyWith(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal);//TextStyle(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal,fontFamily: selectedFontTextStyle.fontFamily);
      //           texts.elementAt(index).textAlign=textAlign;
      //           Navigator.pop(context);
      //         }else {
      //           texts.add(ImageText(
      //               textAlign: textAlign,
      //               value: text.text.toString(),
      //               left: 0,
      //               top: 0,
      //               textStyle: selectedFontTextStyle.copyWith(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal)//TextStyle(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal,fontFamily: selectedFont)
      //           ));
      //           Navigator.pop(context);
      //         }
      //       });
      //     },
      //     child:textToEdit!=null?const Text("Edit"): const Text("Add"),
      //   )
    //   ],
    // );
  }

  Widget resizePhoto(BuildContext context){
    double width=MediaQuery.of(context).size.width, height=MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.all(30),
      child: Column(
        children: [
          Row(
            children: [
              Text("Rotate"),
              Expanded(
                child: Slider(onChanged: (double value) {

                  setState(() {
                    angle=value;
                  });
                },
                  value: angle,
                  max: 6.3,
                  activeColor: Color.fromRGBO(255, 45, 85, 1),
                  thumbColor: Color.fromRGBO(255, 45, 85, 1),
                  inactiveColor: Color.fromRGBO(255, 45, 85, 0.1),
                ),
              ),
            ],
          ),
          SizedBox(
            height: height*0.05,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AppButton(minWidth: width*0.4,borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Replace photo", textColor: Colors.white, textSize: 15 ,onPressed:()async{
                widget.firstImageFile=await getImageFromPhone();
                setState(() {

                });
              }),
              AppButton(minWidth: width*0.4,borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Add cover", textColor: Colors.white, textSize: 15 ,onPressed:() async{
                widget.secondImageFile=await getImageFromPhone();
                setState(() {
                  widget.isSecondeImageSelected=true;
                });
              })
            ],
          )
        ],
      ),
    );
  }

  Widget saveOption(double width,double height){
    return Container(
      padding: EdgeInsets.all(30),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AppButton(minWidth: width*0.4,borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Facebook story", textColor: Colors.white, textSize: 15 ,onPressed:()async{
                takeScreenShot(768, 1360);
              }),
              AppButton(minWidth: width*0.4,borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Facebook post", textColor: Colors.white, textSize: 15 ,onPressed:()async{
                takeScreenShot(1200, 630);
              }),
            ],
          ),
          SizedBox(height: MediaQuery.of(context).size.height*0.05,),
          AppButton(minWidth: width*0.4,borderRadius: 10, borderColor: Colors.white, borderWidth: 0, buttonText: "Facebook cover", textColor: Colors.white, textSize: 15 ,onPressed:()async{
            takeScreenShot(820, 312);
          }),
        ],
      ),
    );
  }

  Future<XFile?> getImageFromPhone() async{
    XFile? image=await ImagePicker().pickImage(source: ImageSource.gallery);
    if(image!.path.endsWith(".png")||
        image.path.endsWith(".jpg")||
        image.path.endsWith(".jpeg")) {
      return image;
    } else {
      return null;
    }
  }



  final List<String> myGoogleFonts = ["Abril Fatface", "Aclonica", "Alegreya Sans", "Architects Daughter", "Archivo", "Archivo Narrow", "Bebas Neue", "Bitter", "Bree Serif", "Bungee", "Cabin", "Cairo", "Coda", "Comfortaa", "Comic Neue", "Cousine", "Croissant One", "Faster One", "Forum", "Great Vibes", "Heebo", "Inconsolata", "Josefin Slab", "Lato", "Libre Baskerville", "Lobster", "Lora", "Merriweather", "Montserrat", "Mukta", "Nunito", "Offside", "Open Sans", "Oswald", "Overlock", "Pacifico", "Playfair Display", "Poppins", "Raleway", "Roboto", "Roboto Mono", "Source Sans Pro", "Space Mono", "Spicy Rice", "Squada One", "Sue Ellen Francisco", "Trade Winds", "Ubuntu", "Varela", "Vollkorn", "Work Sans", "Zilla Slab"];


}
