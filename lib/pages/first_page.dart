import 'dart:io';
import 'package:burgas_photo/model/text_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_font_picker/flutter_font_picker.dart';
import 'package:mdi/mdi.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_picker_platform_interface/src/types/image_source.dart' as ImageSource;
import 'package:matrix_gesture_detector/matrix_gesture_detector.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:async';

import 'package:screenshot/screenshot.dart';

class FirstPage extends StatefulWidget {
  XFile? firstImageFile;
  XFile? secondImageFile;
  bool isFirstImageSelected=false;
  bool isSecondeImageSelected=false;
  double imageWidth=0;
  double imageHeight=0;
  FirstPage({Key? key}) : super(key: key);

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  final screenController=ScreenshotController();
  double horizontalScale=1;
  double verticalScale=1;
  double? width;
  double? height;
  double? imageheight;
  List<ImageText> texts=[];
  double fontSize=10;
  String selectedFont = "Roboto";
  Matrix4? transform;
  Matrix4? scale;
  ScaleUpdateDetails temp=ScaleUpdateDetails();
  double angle=0;


  Future<XFile?> getImageFromPhone() async{
    XFile? image=await ImagePicker().pickImage(source: ImageSource.ImageSource.gallery);
    if(image!.path.endsWith(".png")||
        image.path.endsWith(".jpg")||
        image.path.endsWith(".jpeg")) {
      return image;
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    width=MediaQuery.of(context).size.width;
    height=MediaQuery.of(context).size.height;
    if( widget.imageWidth==widget.imageHeight){
      widget.imageHeight=height!*0.5;
      widget.imageWidth=width!;
    }
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          width: width,
          height: height,
          child: ListView(
            children: [
              !widget.isFirstImageSelected?SizedBox(
                height: height!*0.2,
              ):bar(),
              Container(
                width: width,
                height: height!*0.5,
                decoration: BoxDecoration(
                  color: Colors.grey[100]
                ),
                child: !widget.isFirstImageSelected?
                IconButton(
                    onPressed: () async {
                      widget.firstImageFile= await getImageFromPhone();
                      //widget.firstImageCropped=await cropPhoto(widget.firstImageFile!);
                      setState(() {
                        widget.isFirstImageSelected=true;
                      });
                    },
                    icon:const Icon(Icons.add_circle_rounded)
                ):
                Container(
                  width:width,
                  child: Screenshot(
                    controller: screenController,
                    child: Stack(
                      children: [
                        GestureDetector(
                            onScaleUpdate: (update){
                              setState(() {
                                print(update.toString());
                                if(update.horizontalScale>temp.horizontalScale)
                                  widget.imageWidth=widget.imageWidth+update.horizontalScale;
                                if(update.horizontalScale<temp.horizontalScale)
                                  widget.imageWidth=widget.imageWidth-update.horizontalScale;
                                if(update.verticalScale>temp.verticalScale)
                                  widget.imageHeight=widget.imageHeight+update.verticalScale;
                                if(update.verticalScale<temp.verticalScale)
                                  widget.imageHeight=widget.imageHeight-update.verticalScale;
                                temp=update;
                              });
                            },
                            child: Center(child: SingleChildScrollView(scrollDirection: Axis.horizontal,child: SingleChildScrollView(child: Column(children: [Transform.rotate(angle: angle,child: Image.file(File(widget.firstImageFile!.path),width: widget.imageWidth,height: widget.imageHeight,fit: BoxFit.fill,)),SizedBox(height: 40,)],))))),
                        widget.isSecondeImageSelected?Image.file(File(widget.secondImageFile!.path),width: width,height: imageheight,fit: BoxFit.fill ,):Container(),
                        for(int i=0;i<texts.length;i++)
                          Positioned(
                              // top: texts[i].top,
                              // left: texts[i].left,
                              child: Draggable(
                                feedback:  Text(texts[i].value, style: texts[i].textStyle,textAlign: texts[i].textAlign,),
                                child: InkWell(
                                  onTap: (){
                                    showDialog(context: context, builder:  (BuildContext context) {
                                      return dialog(textToEdit: texts[i],index: i);
                                    },);
                                  },
                                  // GoogleFonts.acme(fontSize: texts[i].fontSize, color: texts[i].fontColor,fontStyle:texts[i].fontStyle,fontWeight: texts[i].fontWeight )
                                  child: Text(texts[i].value, style: texts[i].textStyle,textAlign: texts[i].textAlign,),
                                ),
                                onDragEnd: (drag){
                                  final renderBox=context.findRenderObject() as RenderBox;
                                  Offset offset=renderBox.globalToLocal(drag.offset);
                                  setState(() {
                                    // texts[i].left=offset.dx;
                                    // texts[i].top=offset.dy-height!*0.3;
                                  });
                                },
                              )
                          ),
                      ],
                    )
                  ),
                ),
              ),
              Slider(onChanged: (double value) {
                setState(() {
                  angle=value;
                  print(value);
                });
              },
                value: angle,
            max: 6.3,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget bar(){
    return Padding(
      padding:  EdgeInsets.only(left: width!*0.05,right: width!*0.05),
      child: Container(
        margin: EdgeInsets.only(bottom: height!*0.1,top: height!*0.1),
        padding: EdgeInsets.only(left: width!*0.05,right: width!*0.05),
        width: width,
        decoration:const BoxDecoration(
            color: Colors.black54,
            borderRadius: BorderRadius.all(Radius.circular(30))
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            IconButton(
                onPressed: ()async{
                  var status = await Permission.storage.status;
                  if (!status.isGranted) {
                    await Permission.storage.request();
                  }else {
                    setState(() {
                      showDialog(context: context, builder: (BuildContext context){
                        return saveOption();
                      });
                    });
                  }
                },
                icon: Icon(Icons.ios_share,color: Colors.white,)),
            IconButton(
                onPressed: (){
                  setState(() {
                    showDialog(context: context, builder:  (BuildContext context) {
                      return dialog();
                    },);
                  });
                },
                icon: Icon(Mdi.alphaA,color: Colors.white,)),
            IconButton(
                onPressed: ()async{
                  widget.secondImageFile=await getImageFromPhone();
                  setState(() {
                    widget.isSecondeImageSelected=true;
                  });
                },
                icon: Icon(Icons.photo_library,color: Colors.white,)),
            IconButton(
                onPressed: ()async{
                  widget.firstImageFile= await getImageFromPhone();
                  //widget.firstImageCropped=await cropPhoto(widget.firstImageFile!);
                  setState(() {
                  widget.isFirstImageSelected=true;});
                },
                icon: Icon(Icons.photo,color: Colors.white,)),
          ],
        ),
      ),
    );
  }

  Future<void>takeScreenShot() async{
    final image = await screenController.capture();
    var dt = DateTime.now();
    final result=await ImageGallerySaver.saveImage(image!,name:"screenshoot+${dt.toString()}" ,quality: 100);
    if(result!=null){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("saved! "+result.toString()),
      ));
    }
  }
  Widget dialog({ImageText? textToEdit,int? index}){
    Color selectedColor=Colors.black;
    TextEditingController text=TextEditingController();
    TextStyle selectedFontTextStyle=TextStyle();
    bool alignLeft=true;
    bool alignRight=false;
    bool alignCenter=false;
    bool isBold=false;
    bool isItalic=false;
    if(textToEdit!=null){
      text.text=textToEdit.value;
      fontSize=textToEdit.textStyle!.fontSize!;
      selectedColor=textToEdit.textStyle!.color!;

      if(textToEdit.textStyle!.fontWeight==FontWeight.bold){
        isBold=true;
      }
      if(textToEdit.textStyle!.fontStyle==FontStyle.italic){
        isItalic=true;
      }
    }
    return AlertDialog(

      elevation: 10,
      title: textToEdit==null?Text("Add new Text"):Text("Edit Text"),
      shape:const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          borderSide: BorderSide.none
      ),
      content: StatefulBuilder(
        builder:(BuildContext context, StateSetter setState){
        return Container(
            height: height!*0.3,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextFormField(
                    controller: text,
                    decoration: const InputDecoration(
                        //label: textToEdit==null?Text(""):Text(textToEdit.value),
                        hintText: "Enter your text"
                    ),
                  ),
                  Slider(
                    onChanged: (val){
                      setState(()=>fontSize=val);
                      },
                    value: fontSize,
                    min: 10,
                    max: 80,
                  ),
                  Row(children: [
                    MaterialButton(
                      onPressed: (){
                        showDialog(context: context, builder: (BuildContext context){
                          return AlertDialog(
                            title: Text("Pick Color"),
                            content: SingleChildScrollView(
                              child: ColorPicker(
                                pickerColor: Color(0xff443a49),
                                onColorChanged: (Color value) {
                                  selectedColor=value;
                                },
                              ),
                            ),
                            actions: [
                              MaterialButton(
                                onPressed: (){
                                  Navigator.pop(context);
                                },
                                child: Text("Done"),
                              )
                            ],

                          );
                        });},
                      child: Text("Pick Color"),),
                    MaterialButton(
                      onPressed: () {
                        showDialog(context: context, builder: (BuildContext context){
                          return AlertDialog(
                            content: SingleChildScrollView(
                              child: FontPicker(
                                googleFonts: myGoogleFonts,
                                showInDialog: true,
                                onFontChanged: (font){selectedFont=font.fontFamily;selectedFontTextStyle=font.toTextStyle();},
                              ),
                            ),
                          );
                        });
                      },
                      child:const Text("Pick Family"),),
                  ],),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(

                        onTap: (){
                          alignLeft=true;
                          alignRight=false;
                          alignCenter=false;
                        },
                        child: Icon(Mdi.formatAlignLeft),
                      ),
                      InkWell(
                        onTap: (){
                          alignLeft=false;
                          alignRight=false;
                          alignCenter=true;
                        },
                        child: const Icon(Mdi.formatAlignCenter),
                      ),
                      InkWell(
                        onTap: (){
                          alignLeft=false;
                          alignRight=true;
                          alignCenter=false;
                        },
                        child:const Icon(Mdi.formatAlignRight),
                      ),
                      InkWell(
                        onTap: (){
                          isBold=!isBold;
                        },
                        child: const Icon(Mdi.formatBold),
                      ),
                      InkWell(
                        onTap: (){
                          isItalic=!isItalic;
                        },
                        child: const Icon(Mdi.formatItalic),
                      ),
                    ],
                  ),
                ],
              ),
            )

        );}
      ),
      actions: [
        MaterialButton(
          onPressed: (){
            setState(() {
              TextAlign? textAlign;
              if(alignCenter){
                textAlign=TextAlign.center;
              }
              else if(alignRight){
                textAlign=TextAlign.end;
              }
              else if(alignLeft){
                textAlign=TextAlign.left;
              }
              if(textToEdit!=null&&index!=null){
                texts.elementAt(index).value=text.text.toString();
                texts.elementAt(index).textStyle=selectedFontTextStyle.copyWith(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal);//TextStyle(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal,fontFamily: selectedFontTextStyle.fontFamily);
                texts.elementAt(index).textAlign=textAlign;
                Navigator.pop(context);
              }else {
                texts.add(ImageText(
                    textAlign: textAlign,
                    value: text.text.toString(),
                    // left: 0,
                    // top: 0,
                    textStyle: selectedFontTextStyle.copyWith(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal)//TextStyle(fontSize: fontSize,color: selectedColor,fontWeight: isBold?FontWeight.bold:FontWeight.normal,fontStyle: isItalic?FontStyle.italic:FontStyle.normal,fontFamily: selectedFont)
                ));
                Navigator.pop(context);
              }
            });
          },
          child:textToEdit!=null?const Text("Edit"): const Text("Add"),
        )
      ],
    );
  }

  Widget saveOption(){
    return AlertDialog(
      content: Container(
        height: height!*0.09,
        child: Column(
          children: [
            InkWell(
              onTap: (){
                takeScreenShot();
                Navigator.pop(context);
              },
              child: Container(
                width: width,
                child: Text("Facebook Post",style: TextStyle(fontWeight: FontWeight.bold),),
              ),
            ),
            Divider(thickness: 1,),
            InkWell(
              onTap: (){
                widget.imageHeight=height!;
                takeScreenShot();
                Navigator.pop(context);
              },
              child: Container(
                width: width,
                child: Text("Facebook Story",style: TextStyle(fontWeight: FontWeight.bold),),
              ),
            ),
          ],
        ),
      ),
    );
  }

  final List<String> myGoogleFonts = ["Abril Fatface", "Aclonica", "Alegreya Sans", "Architects Daughter", "Archivo", "Archivo Narrow", "Bebas Neue", "Bitter", "Bree Serif", "Bungee", "Cabin", "Cairo", "Coda", "Comfortaa", "Comic Neue", "Cousine", "Croissant One", "Faster One", "Forum", "Great Vibes", "Heebo", "Inconsolata", "Josefin Slab", "Lato", "Libre Baskerville", "Lobster", "Lora", "Merriweather", "Montserrat", "Mukta", "Nunito", "Offside", "Open Sans", "Oswald", "Overlock", "Pacifico", "Playfair Display", "Poppins", "Raleway", "Roboto", "Roboto Mono", "Source Sans Pro", "Space Mono", "Spicy Rice", "Squada One", "Sue Ellen Francisco", "Trade Winds", "Ubuntu", "Varela", "Vollkorn", "Work Sans", "Zilla Slab"];
}
