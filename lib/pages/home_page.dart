import 'package:burgas_photo/pages/edit_page.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:page_transition/page_transition.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  XFile? firstImageFile;

  Future<XFile?> getImageFromPhone() async{
    XFile? image=await ImagePicker().pickImage(source: ImageSource.gallery);
    if(image!.path.endsWith(".png")||
        image.path.endsWith(".jpg")||
        image.path.endsWith(".jpeg")) {
      return image;
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    double width=MediaQuery.of(context).size.width;
    double height=MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        decoration:const BoxDecoration(
            image: DecorationImage(image: AssetImage("assets/images/home_screen.png"),fit: BoxFit.fill)
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              child: Image.asset("assets/images/pick_image.png",width: 170,height: 170,fit: BoxFit.fill,),
              onTap: () async {
                firstImageFile= await getImageFromPhone();
                Navigator.of(context).push(PageTransition(child: EditPage(firstImageFile: firstImageFile,width: width,height: height*0.5,), type: PageTransitionType.rightToLeft));
              },
            )
          ],
        ),
      ),
    );
  }
}
