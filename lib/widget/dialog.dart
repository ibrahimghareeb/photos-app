import 'package:flutter/material.dart';

class AddTextDialog extends StatelessWidget {
  AddTextDialog({Key? key}) : super(key: key);
  TextEditingController text=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 10,
      title: Text("Add new Text"),
      shape: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          borderSide: BorderSide.none
      ),
      content: Container(
        child: TextFormField(
          controller: text,

        ),
      ),
      actions: [
        MaterialButton(
          onPressed: (){

          },
          child: const Text("Add"),
        )
      ],
    );
  }
}
