
import 'package:flutter/material.dart';



class AppButton extends StatelessWidget {
  late final GestureTapCallback  onPressed;
  late double minWidth;
  late double borderRadius;
  late Color borderColor;
  late double borderWidth;
  late String buttonText;
  late Color textColor;
  late double textSize;


  AppButton({required this.minWidth,required this.borderRadius,required this.borderColor,required this.borderWidth,required this.buttonText,required this.textColor,required this.textSize,required this.onPressed});


  @override
  Widget build(BuildContext context) {
    return  MaterialButton(
      minWidth: minWidth,
      height: MediaQuery.of(context).size.height*0.08,
      child:  Text(buttonText,style: TextStyle(
          color: textColor,
          fontSize: textSize
      ),),
      color: Color.fromRGBO(255, 45, 85, 1),
      shape:  OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
          borderSide: BorderSide(
              width: borderWidth,
              color: borderColor
          )
      ),
      onPressed: onPressed,


    );
  }
}
